#!/usr/bin/env bash

# Requires BEARER env var to be set.

set -euo pipefail

nlp/nlp < nlp/grammar_dialogflow_small.txt | sed -e 's/\(.*\)/{"isTemplate":true,"data":[{"text":"\1"}]},/' -e '$s/,$//' | tr -d '\n' > $TMPDIR/templates.txt

sed "/\"userSays\":/r $TMPDIR/templates.txt" tools/dialogflow_v1_GetAirQuality.json > $TMPDIR/dialogflow_v1_GetAirQuality.json

curl -X PUT 'https://api.dialogflow.com/v1/intents/98d96fde-9765-4c8c-8378-30461738beef' -H "Authorization: Bearer $BEARER" -H 'Content-Type: application/json' --data "@$TMPDIR/dialogflow_v1_GetAirQuality.json"
