#!/usr/bin/env bash

set -euo pipefail

nlp/nlp < nlp/grammar_alexa_small.txt | sed -e 's/\(.*\)/"\1",/' -e '$s/,$//' | tr -d '\n' > $TMPDIR/templates.txt

sed "/\"samples\":/r $TMPDIR/templates.txt" tools/alexa_schema.json > $TMPDIR/alexa_schema.json
