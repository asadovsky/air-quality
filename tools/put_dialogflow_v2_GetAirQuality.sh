#!/usr/bin/env bash

# Requires GOOGLE_APPLICATION_CREDENTIALS env var to be set.

set -euo pipefail

nlp/nlp < nlp/grammar_dialogflow_small.txt | sed -e 's/\(.*\)/{"type":"TEMPLATE","parts":[{"text":"\1"}]},/' -e '$s/,$//' | tr -d '\n' > $TMPDIR/templates.txt

sed "/\"trainingPhrases\":/r $TMPDIR/templates.txt" tools/dialogflow_v2_GetAirQuality.json > $TMPDIR/dialogflow_v2_GetAirQuality.json

curl -X PATCH 'https://dialogflow.googleapis.com/v2/projects/air-quality-13001/agent/intents/98d96fde-9765-4c8c-8378-30461738beef' -H "Authorization: Bearer $(gcloud auth application-default print-access-token)" -H 'Content-Type: application/json' --data "@$TMPDIR/dialogflow_v2_GetAirQuality.json"
