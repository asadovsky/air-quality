#!/usr/bin/env bash

# Requires BEARER env var to be set.

set -euo pipefail

curl -X GET 'https://api.dialogflow.com/v1/intents/98d96fde-9765-4c8c-8378-30461738beef' -H "Authorization: Bearer $BEARER" -H 'Content-Type: application/json'
