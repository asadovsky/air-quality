#!/usr/bin/env bash

# Requires GOOGLE_APPLICATION_CREDENTIALS env var to be set.

set -euo pipefail

curl -X GET 'https://dialogflow.googleapis.com/v2/projects/air-quality-13001/agent/intents/98d96fde-9765-4c8c-8378-30461738beef?intentView=INTENT_VIEW_FULL' -H "Authorization: Bearer $(gcloud auth application-default print-access-token)" -H 'Content-Type: application/json'
