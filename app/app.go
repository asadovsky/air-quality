package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"strings"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
)

var m Model

// joinIgnoreEmpty is like strings.Join, but ignores empty input strings.
func joinIgnoreEmpty(a []string, sep string) string {
	b := make([]string, 0, len(a))
	for _, v := range a {
		if v != "" {
			b = append(b, v)
		}
	}
	return strings.Join(b, sep)
}

func getAddressFromSlots(zipCode, address, city, state string) (string, error) {
	trim := func(s string) string {
		return strings.Trim(s, " ")
	}
	if res := trim(zipCode); res != "" {
		return res, nil
	}
	if res := joinIgnoreEmpty([]string{trim(address), trim(city), trim(state)}, ", "); res != "" {
		return res, nil
	}
	return "", errors.New("No address")
}

func getLongName(geoRes *GeocodeResult, t string) string {
	for _, ac := range geoRes.AddressComponents {
		for _, ty := range ac.Types {
			if t == ty {
				return ac.LongName
			}
		}
	}
	return ""
}

type foreignCountryError struct{}

func (err *foreignCountryError) Error() string {
	return ""
}

func getAQI(geoRes *GeocodeResult, c context.Context) (*GetAQIResponse, error) {
	if getLongName(geoRes, "country") != "United States" {
		// TODO: Fall back to data from http://aqicn.org/api/.
		return nil, &foreignCountryError{}
	}
	loc := geoRes.Geometry.Location
	return m.GetAQI(loc.Lat, loc.Lng, c)
}

func getGeoName(geoRes *GeocodeResult) string {
	res := ""
	setRes := func(t string) {
		if res == "" {
			res = getLongName(geoRes, t)
		}
	}
	setRes("point_of_interest")           // e.g. "Yosemite"
	setRes("natural_feature")             // e.g. "Lake Tahoe"
	setRes("sublocality_level_1")         // e.g. "Queens"
	setRes("colloquial_area")             // e.g. "San Francisco Bay Area"
	setRes("locality")                    // city
	setRes("administrative_area_level_2") // county
	res = joinIgnoreEmpty([]string{
		res,
		getLongName(geoRes, "administrative_area_level_1"), // state
	}, ", ")
	return res
}

func makeAQISpeech(address string, c context.Context) (string, error) {
	geoRes, err := Geocode(address, c)
	if err != nil {
		return "", err
	}
	aqiRes, err := getAQI(geoRes, c)
	if err != nil {
		if _, ok := err.(*foreignCountryError); ok {
			return "Sorry, but locations outside the United States are not yet supported.", nil
		}
		return "", err
	}
	geoName := getGeoName(geoRes)
	if geoName == "" {
		geoName = "there"
		log.Errorf(c, "getGeoName failed for address=%q, geoRes=%v", address, geoRes)
	} else {
		geoName = "for " + geoName
	}
	return fmt.Sprintf("The air quality index %s is %d, which is considered %s.", geoName, aqiRes.AQI, DescribeAQI(aqiRes.AQI)), nil
}

func writeJSON(res interface{}, w http.ResponseWriter) {
	body, err := json.Marshal(res)
	checkError(err)
	w.Header().Set("Content-Type", "application/json")
	w.Write(body)
}

////////////////////////////////////////////////////////////////////////////////
// Alexa-specific code

func buildAlexaResponse(speech string, shouldEndSession bool) *AlexaResponse {
	res := AlexaResponse{}
	res.Version = "1.0"
	if speech != "" {
		res.Response.OutputSpeech = &AlexaResOutputSpeech{
			Type: "PlainText",
			Text: speech,
		}
	}
	res.Response.ShouldEndSession = shouldEndSession
	return &res
}

func handleAlexaLaunch(r *AlexaRequest, c context.Context) (*AlexaResponse, error) {
	return buildAlexaResponse(speechLaunch, false), nil
}

func getAddressFromAlexaRequest(r *AlexaRequest) (string, error) {
	s := &r.Request.Intent.Slots
	return getAddressFromSlots("", s.PostalAddress.Value, s.City.Value, s.State.Value)
}

func handleAlexaIntentGetAirQuality(r *AlexaRequest, c context.Context) (*AlexaResponse, error) {
	speech := ""
	shouldEndSession := true
	if address, err := getAddressFromAlexaRequest(r); err == nil {
		speech, err = makeAQISpeech(address, c)
		if err != nil {
			return nil, err
		}
	} else {
		// User did not specify address in query. Use device location.
		// TODO: Get device location.
		// https://developer.amazon.com/docs/custom-skills/device-address-api.html
		speech = speechNeedLocation
		shouldEndSession = false
	}
	return buildAlexaResponse(speech, shouldEndSession), nil
}

func handleAlexaIntentExit(r *AlexaRequest, c context.Context) (*AlexaResponse, error) {
	return buildAlexaResponse("", true), nil
}

func handleAlexaInternal(r *AlexaRequest, c context.Context) (*AlexaResponse, error) {
	switch r.Request.Type {
	case "LaunchRequest":
		return handleAlexaLaunch(r, c)
	case "IntentRequest":
		switch r.Request.Intent.Name {
		case "GetAirQuality":
			return handleAlexaIntentGetAirQuality(r, c)
		case "AMAZON.CancelIntent":
			return handleAlexaIntentExit(r, c)
		case "AMAZON.StopIntent":
			return handleAlexaIntentExit(r, c)
		default:
			return nil, fmt.Errorf("Unknown intent: %s", r.Request.Intent.Name)
		}
	case "SessionEndedRequest":
		return handleAlexaIntentExit(r, c)
	default:
		return nil, fmt.Errorf("Unknown request type: %s", r.Request.Type)
	}
}

func handleAlexa(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	checkError(err)
	c := appengine.NewContext(r)
	log.Debugf(c, string(b))
	if err := verifyAlexaRequest(b, r.Header.Get("SignatureCertChainUrl"), r.Header.Get("Signature"), c); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	var req AlexaRequest
	checkError(json.Unmarshal(b, &req))
	if req.Session.Application.ApplicationID != alexaAppID {
		http.Error(w, fmt.Sprintf("Wrong application ID: %s", req.Session.Application.ApplicationID), http.StatusBadRequest)
		return
	}
	if d := time.Since(req.Request.Timestamp); math.Abs(d.Seconds()) > 150 {
		http.Error(w, fmt.Sprintf("Bad timestamp: %s", req.Request.Timestamp), http.StatusBadRequest)
		return
	}
	res, err := handleAlexaInternal(&req, c)
	checkError(err)
	writeJSON(res, w)
}

////////////////////////////////////////////////////////////////////////////////
// Dialogflow-specific code

func buildDialogflowV2Response(speech string, shouldEndSession bool) *DialogflowV2Response {
	res := DialogflowV2Response{}
	res.FulfillmentText = speech
	res.Payload.Google.ExpectUserResponse = !shouldEndSession
	return &res
}

func queryContainsBayArea(r *DialogflowV2Request) bool {
	return strings.Contains(strings.ToLower(r.QueryResult.QueryText), "bay area")
}

func getAddressFromDialogflowV2Request(r *DialogflowV2Request) (string, error) {
	p := &r.QueryResult.Parameters
	res, err := getAddressFromSlots(p.ZipCode, p.Address, p.GeoCity, p.GeoStateUS)
	// Spot fix for "Bay Area" queries.
	if queryContainsBayArea(r) && (res == "" || strings.ToLower(res) == "bay") && p.Address == "" && p.GeoStateUS == "" && p.ZipCode == "" {
		return "San Francisco Bay Area", nil
	}
	return res, err
}

func handleDialogflowV2IntentLaunch(r *DialogflowV2Request, c context.Context) (*DialogflowV2Response, error) {
	return buildDialogflowV2Response(speechLaunch, false), nil
}

func handleDialogflowV2IntentFallback(r *DialogflowV2Request, c context.Context) (*DialogflowV2Response, error) {
	// Spot fix for "Bay Area" queries.
	if queryContainsBayArea(r) {
		return handleDialogflowV2IntentGetAirQuality(r, c)
	}
	return buildDialogflowV2Response(speechFallback, false), nil
}

func handleDialogflowV2IntentGetAirQuality(r *DialogflowV2Request, c context.Context) (*DialogflowV2Response, error) {
	speech := ""
	shouldEndSession := true
	if address, err := getAddressFromDialogflowV2Request(r); err == nil {
		speech, err = makeAQISpeech(address, c)
		if err != nil {
			return nil, err
		}
	} else {
		// User did not specify address in query. Use device location.
		// TODO: Get the device location.
		// https://developers.google.com/actions/assistant/helpers
		speech = speechNeedLocation
		shouldEndSession = false
	}
	return buildDialogflowV2Response(speech, shouldEndSession), nil
}

func handleDialogflowV2Internal(r *DialogflowV2Request, c context.Context) (*DialogflowV2Response, error) {
	switch r.QueryResult.Action {
	case "Launch":
		return handleDialogflowV2IntentLaunch(r, c)
	case "Fallback":
		return handleDialogflowV2IntentFallback(r, c)
	case "GetAirQuality":
		return handleDialogflowV2IntentGetAirQuality(r, c)
	default:
		return nil, fmt.Errorf("Unknown intent: %s", r.QueryResult.Action)
	}
}

func handleDialogflowV1(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	checkError(err)
	c := appengine.NewContext(r)
	log.Debugf(c, string(b))
	var v1Req DialogflowV1Request
	checkError(json.Unmarshal(b, &v1Req))
	var v2Req DialogflowV2Request
	v2Req.QueryResult.QueryText = v1Req.Result.ResolvedQuery
	v2Req.QueryResult.Action = v1Req.Result.Action
	v2Req.QueryResult.Parameters = v1Req.Result.Parameters
	res, err := handleDialogflowV2Internal(&v2Req, c)
	checkError(err)
	writeJSON(DialogflowV1Response{
		Speech:      res.FulfillmentText,
		DisplayText: res.FulfillmentText,
		Data:        res.Payload,
	}, w)
}

func handleDialogflowV2(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	checkError(err)
	c := appengine.NewContext(r)
	log.Debugf(c, string(b))
	var req DialogflowV2Request
	checkError(json.Unmarshal(b, &req))
	res, err := handleDialogflowV2Internal(&req, c)
	checkError(err)
	writeJSON(res, w)
}

////////////////////////////////////////////////////////////////////////////////
// Other request-handling code

func handleGetAirQuality(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	geoRes, err := Geocode(r.FormValue("address"), c)
	checkError(err)
	aqiRes, err := getAQI(geoRes, c)
	checkError(err)
	writeJSON(aqiRes, w)
}

func handlePrintModel(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, m.DebugString())
}

func handleWarmup(w http.ResponseWriter, r *http.Request) {
	checkError(m.Update(appengine.NewContext(r)))
}

func init() {
	m = NewAirNowModel()
	http.Handle("/api/get-air-quality", wrapHandler(handleGetAirQuality))
	http.Handle("/api/alexa", wrapHandler(handleAlexa))
	http.Handle("/api/dialogflow-v1", wrapHandler(handleDialogflowV1))
	http.Handle("/api/dialogflow-v2", wrapHandler(handleDialogflowV2))
	http.Handle("/admin/print-model", wrapHandler(handlePrintModel))
	http.Handle("/_ah/warmup", wrapHandler(handleWarmup))
}

func main() {
	appengine.Main()
}
