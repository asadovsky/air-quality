package main

const alexaAppID = "amzn1.ask.skill.80e27fd2-b416-4fa3-9164-65fe542ae9ff"

// Format: https://docs.airnowapi.org/docs/HourlyDataFactSheet.pdf
const reportingAreaFile = "https://s3-us-west-1.amazonaws.com//files.airnowtech.org/airnow/today/reportingarea.dat"

// Google API URLs.
const geocodeURL = "https://maps.googleapis.com/maps/api/geocode/json"

// Speech strings.
const speechFallback = "Sorry, I didn't get that. You can ask, \"What's the air quality in San Francisco?\""
const speechLaunch = "Hi! Ask me about the air quality."
const speechNeedLocation = "Please specify a location. For example, you can ask, \"What's the air quality in San Francisco?\""
