module bitbucket.org/asadovsky/air-quality/app

go 1.12

require (
	github.com/golang/geo v0.0.0-20190507233405-a0e886e97a51
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80
	google.golang.org/appengine v1.6.1
)
