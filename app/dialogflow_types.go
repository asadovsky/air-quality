package main

type DialogflowV1Request struct {
	Result struct {
		ResolvedQuery string `json:"resolvedQuery"`
		Action        string `json:"action"`
		Parameters    struct {
			Address    string `json:"address"`
			GeoCity    string `json:"geo-city"`
			GeoStateUS string `json:"geo-state-us"`
			ZipCode    string `json:"zip-code"`
		} `json:"parameters"`
	} `json:"result"`
}

type DialogflowV1Response struct {
	Speech      string `json:"speech"`
	DisplayText string `json:"displayText"`
	Data        struct {
		Google struct {
			ExpectUserResponse bool `json:"expect_user_response"`
		} `json:"google"`
	} `json:"data"`
}

type DialogflowV2Request struct {
	QueryResult struct {
		QueryText  string `json:"queryText"`
		Action     string `json:"action"`
		Parameters struct {
			Address    string `json:"address"`
			GeoCity    string `json:"geo-city"`
			GeoStateUS string `json:"geo-state-us"`
			ZipCode    string `json:"zip-code"`
		} `json:"parameters"`
	} `json:"queryResult"`
}

type DialogflowV2Response struct {
	FulfillmentText string `json:"fulfillmentText"`
	Payload         struct {
		Google struct {
			ExpectUserResponse bool `json:"expect_user_response"`
		} `json:"google"`
	} `json:"payload"`
}
