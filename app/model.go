package main

import (
	"time"

	"golang.org/x/net/context"
)

type GetAQIResponse struct {
	AQI        int32
	Pollutants map[string]int32
}

// Model provides air quality data.
type Model interface {
	// GetAQI returns the current AQI for the given latlng.
	GetAQI(lat, lng float64, c context.Context) (*GetAQIResponse, error)

	// LastUpdated returns the time when the model was last updated.
	LastUpdated() time.Time

	// Update updates the model.
	Update(context.Context) error

	// DebugString returns a string representation of the model.
	DebugString() string
}

// DescribeAQI returns a text description of the "level of concern" for the
// given AQI value.
func DescribeAQI(aqi int32) string {
	switch {
	case aqi <= 50: // Green
		return "Good"
	case aqi <= 100: // Yellow
		return "Moderate"
	case aqi <= 150: // Orange
		return "Unhealthy for Sensitive Groups"
	case aqi <= 200: // Red
		return "Unhealthy"
	case aqi <= 300: // Purple
		return "Very Unhealthy"
	default: // Maroon
		return "Hazardous"
	}
}
