package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"runtime/debug"

	"google.golang.org/appengine"
	"google.golang.org/appengine/log"
)

type errorWithStackTrace struct {
	stack []byte // from debug.Stack()
	err   error
}

func (err *errorWithStackTrace) Error() string {
	if err.stack != nil {
		return fmt.Sprintf("%s\n%v", err.stack, err.err)
	}
	return fmt.Sprint(err.err)
}

func debugStack() []byte {
	if appengine.IsDevAppServer() {
		return debug.Stack()
	}
	return nil
}

func checkError(err error) {
	if err != nil {
		panic(&errorWithStackTrace{
			stack: debugStack(),
			err:   err,
		})
	}
}

func assert(condition bool, v ...interface{}) {
	if !condition {
		panic(&errorWithStackTrace{
			stack: debugStack(),
			err:   errors.New(fmt.Sprint(v...)),
		})
	}
}

func serveError(w http.ResponseWriter, data interface{}) {
	http.Error(w, fmt.Sprint(data), http.StatusInternalServerError)
}

func wrapHandler(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// http://blog.golang.org/2010/08/defer-panic-and-recover.html
		defer func() {
			if data := recover(); data != nil {
				log.Errorf(appengine.NewContext(r), fmt.Sprint(data))
				serveError(w, data)
			}
		}()
		h(w, r)
	}
}

func getBody(client *http.Client, url string) ([]byte, error) {
	resp, err := client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode != http.StatusOK {
		bodyStr := ""
		if err == nil {
			bodyStr = fmt.Sprintf(": %s", body)
		}
		return nil, fmt.Errorf("Cannot GET %s (%v)%s", url, resp.StatusCode, bodyStr)
	}
	if err != nil {
		return nil, err
	}
	return body, nil
}
