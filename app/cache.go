package main

import (
	"time"

	"golang.org/x/net/context"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/memcache"
)

type CacheItem struct {
	Value      []byte
	Expiration time.Time
}

func cacheGet(c context.Context, key string) *memcache.Item {
	// Read from memcache. If that fails, read from datastore.
	item, err := memcache.Get(c, key)
	if err == nil {
		return item
	}
	if err != memcache.ErrCacheMiss {
		log.Warningf(c, "memcache.Get failed: %v", err)
	}
	dsKey := datastore.NewKey(c, "CacheItem", key, 0, nil)
	dsItem := &CacheItem{}
	err = datastore.Get(c, dsKey, dsItem)
	if err == nil {
		if time.Now().After(dsItem.Expiration) {
			err = datastore.ErrNoSuchEntity
		} else {
			// Update memcache.
			if err := memcache.Set(c, &memcache.Item{
				Key:        key,
				Value:      dsItem.Value,
				Expiration: dsItem.Expiration.Sub(time.Now()),
			}); err != nil {
				log.Warningf(c, "memcache.Set failed: %v", err)
			}
			return &memcache.Item{Key: key, Value: dsItem.Value}
		}
	}
	if err != datastore.ErrNoSuchEntity {
		log.Warningf(c, "datastore.Get failed: %v", err)
	}
	return nil
}

func cacheSet(c context.Context, item *memcache.Item) {
	// Write to memcache and datastore.
	if err := memcache.Set(c, item); err != nil {
		log.Warningf(c, "memcache.Set failed: %v", err)
	}
	dsKey := datastore.NewKey(c, "CacheItem", item.Key, 0, nil)
	dsItem := &CacheItem{
		Value:      item.Value,
		Expiration: time.Now().Add(item.Expiration),
	}
	if _, err := datastore.Put(c, dsKey, dsItem); err != nil {
		log.Warningf(c, "datastore.Put failed: %v", err)
	}
}

func GetWithCache(c context.Context, key string, get func() ([]byte, error)) ([]byte, error) {
	if item := cacheGet(c, key); item != nil {
		return item.Value, nil
	}
	value, err := get()
	if err != nil {
		return nil, err
	}
	cacheSet(c, &memcache.Item{
		Key:        key,
		Value:      value,
		Expiration: 30 * 24 * time.Hour,
	})
	return value, nil
}
