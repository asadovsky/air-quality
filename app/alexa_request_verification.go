package main

import (
	"crypto"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
	"net/url"
	"strings"

	"golang.org/x/net/context"
	"google.golang.org/appengine/urlfetch"
)

func verifyCertChainURL(certChainURL string) error {
	u, err := url.Parse(certChainURL)
	if err != nil {
		return err
	}
	if u.Scheme != "https" {
		return fmt.Errorf("Bad scheme: %s", u.Scheme)
	}
	if u.Hostname() != "s3.amazonaws.com" {
		return fmt.Errorf("Bad hostname: %s", u.Hostname())
	}
	if u.Port() != "" && u.Port() != "443" {
		return fmt.Errorf("Bad port: %s", u.Port())
	}
	if !strings.HasPrefix(u.Path, "/echo.api/") {
		return fmt.Errorf("Bad path: %s", u.Path)
	}
	return nil
}

func verifyAlexaRequest(body []byte, certChainURL, signature string, c context.Context) error {
	if err := verifyCertChainURL(certChainURL); err != nil {
		return err
	}
	pemCerts, err := getBody(urlfetch.Client(c), certChainURL)
	if err != nil {
		return err
	}
	block, _ := pem.Decode(pemCerts)
	if block == nil {
		return errors.New("Failed to decode cert chain")
	}
	cert, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return err
	}
	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemCerts) {
		return errors.New("Failed to add certs to pool")
	}
	if _, err := cert.Verify(x509.VerifyOptions{DNSName: "echo-api.amazon.com", Intermediates: certPool}); err != nil {
		return err
	}
	encryptedSig, err := base64.StdEncoding.DecodeString(signature)
	if err != nil {
		return err
	}
	hash := sha1.Sum(body)
	if err := rsa.VerifyPKCS1v15(cert.PublicKey.(*rsa.PublicKey), crypto.SHA1, hash[:], encryptedSig); err != nil {
		return err
	}
	return nil
}
