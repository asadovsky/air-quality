package main

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strings"

	"golang.org/x/net/context"
	"google.golang.org/appengine/urlfetch"
)

type GeocodeResult struct {
	AddressComponents []struct {
		LongName  string   `json:"long_name"`
		ShortName string   `json:"short_name"`
		Types     []string `json:"types"`
	} `json:"address_components"`
	Geometry struct {
		Location struct {
			Lat float64 `json:"lat"`
			Lng float64 `json:"lng"`
		} `json:"location"`
	} `json:"geometry"`
}

type GeocodeResponse struct {
	Results []GeocodeResult `json:"results"`
	Status  string          `json:"status"`
}

func getGeocodeResult(buf []byte) (*GeocodeResult, error) {
	var r GeocodeResponse
	if err := json.Unmarshal(buf, &r); err != nil {
		return nil, err
	}
	if r.Status != "OK" || len(r.Results) == 0 {
		return nil, fmt.Errorf("getGeocodeResult failed: %v", r)
	}
	return &r.Results[0], nil
}

func Geocode(address string, c context.Context) (*GeocodeResult, error) {
	address = strings.ToLower(strings.Trim(address, " ")) // canonicalize
	// TODO: Add "geocode:" prefix to cache key.
	body, err := GetWithCache(c, address, func() ([]byte, error) {
		u, err := url.Parse(geocodeURL)
		if err != nil {
			return nil, err
		}
		v := url.Values{}
		v.Set("address", address)
		v.Set("key", apiKeyGoogle)
		u.RawQuery = v.Encode()
		body, err := getBody(urlfetch.Client(c), u.String())
		if err != nil {
			return nil, err
		}
		if _, err := getGeocodeResult(body); err != nil {
			return nil, err
		}
		return body, nil
	})
	if err != nil {
		return nil, err
	}
	return getGeocodeResult(body)
}
