package main

import (
	"bufio"
	"bytes"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang/geo/s1"
	"github.com/golang/geo/s2"
	"golang.org/x/net/context"
	_ "google.golang.org/appengine/log"
	"google.golang.org/appengine/urlfetch"
)

type location struct {
	lat float64
	lng float64
}

type paramData struct {
	value       int32
	description string
}

type siteData struct {
	location *location
	params   map[string]*paramData
}

type airNowModel struct {
	lastUpdated time.Time
	sites       map[string]*siteData
}

func NewAirNowModel() Model {
	return &airNowModel{sites: map[string]*siteData{}}
}

func (m *airNowModel) GetAQI(lat, lng float64, c context.Context) (*GetAQIResponse, error) {
	if time.Now().Sub(m.LastUpdated()) > 10*time.Minute {
		if err := m.Update(c); err != nil {
			return nil, err
		}
	}
	latlng := s2.LatLngFromDegrees(lat, lng)
	// Get the siteData for the closest site.
	var minSiteData *siteData
	var minDistance s1.Angle
	for _, sd := range m.sites {
		d := latlng.Distance(s2.LatLngFromDegrees(sd.location.lat, sd.location.lng))
		if minSiteData == nil || d < minDistance {
			minSiteData = sd
			minDistance = d
		}
	}
	// Return the max AQI value across params (e.g. OZONE, PM2.5) for this site.
	res := GetAQIResponse{Pollutants: map[string]int32{}}
	for paramName, pd := range minSiteData.params {
		res.Pollutants[paramName] = pd.value
		if pd.value > res.AQI {
			res.AQI = pd.value
		}
	}
	return &res, nil
}

func (m *airNowModel) LastUpdated() time.Time {
	return m.lastUpdated
}

func (m *airNowModel) Update(c context.Context) error {
	body, err := getBody(urlfetch.Client(c), reportingAreaFile)
	if err != nil {
		return err
	}
	sites := map[string]*siteData{}
	scanner := bufio.NewScanner(bytes.NewReader(body))
	for scanner.Scan() {
		text := scanner.Text()
		parts := strings.Split(text, "|")
		// Check that all required fields are present.
		for _, i := range []int{7, 8, 9, 10, 11} {
			if parts[i] == "" {
				return fmt.Errorf("Missing field %d: %s", i, text)
			}
		}
		// Skip records where the timestamp is missing. From manual inspection,
		// these records appear to contain incorrect param values. Note that some
		// locations only have missing-timestamp records. From manual inspection,
		// these records are not included in the data shown on https://airnow.gov/.
		if parts[2] == "" {
			continue
		}
		// Skip lines where the param value is missing.
		if parts[12] == "" {
			continue
		}
		siteName := fmt.Sprintf("%s, %s", parts[7], parts[8])
		lat, err := strconv.ParseFloat(parts[9], 32)
		if err != nil {
			return err
		}
		lng, err := strconv.ParseFloat(parts[10], 32)
		if err != nil {
			return err
		}
		paramName := parts[11]
		value, err := strconv.ParseInt(parts[12], 10, 32)
		if err != nil {
			return err
		}
		sd, ok := sites[siteName]
		if ok {
			// Verify location.
			loc := location{lat: lat, lng: lng}
			if loc != *sd.location {
				return fmt.Errorf("Unexpected location: %s", text)
			}
		} else {
			sd = &siteData{
				location: &location{lat: lat, lng: lng},
				params:   map[string]*paramData{},
			}
			sites[siteName] = sd
		}
		if pd, ok := sd.params[paramName]; ok {
			if pd.value == int32(value) {
				continue
			}
			return fmt.Errorf("Duplicate param %s: %s", paramName, text)
		}
		sd.params[paramName] = &paramData{
			value:       int32(value),
			description: parts[13],
		}
	}
	m.lastUpdated = time.Now()
	m.sites = sites
	return nil
}

func (m *airNowModel) DebugString() string {
	buf := bytes.Buffer{}
	for siteName, sd := range m.sites {
		parts := []string{siteName}
		parts = append(parts, fmt.Sprintf("%f", sd.location.lat))
		parts = append(parts, fmt.Sprintf("%f", sd.location.lng))
		for paramName, pd := range sd.params {
			parts = append(parts, fmt.Sprintf("%s:%d", paramName, pd.value))
		}
		buf.WriteString(strings.Join(parts, "|"))
		buf.WriteString("\n")
	}
	return buf.String()
}
