package main

import (
	"time"
)

// Generated using: https://mholt.github.io/json-to-go/
type AlexaRequest struct {
	Session struct {
		Application struct {
			ApplicationID string `json:"applicationId"`
		} `json:"application"`
	} `json:"session"`
	Context struct {
		System struct {
			Device struct {
				DeviceID string `json:"deviceId"`
			} `json:"device"`
			APIEndpoint    string `json:"apiEndpoint"`
			APIAccessToken string `json:"apiAccessToken"`
		} `json:"System"`
	} `json:"context"`
	Request struct {
		Type      string    `json:"type"`
		Timestamp time.Time `json:"timestamp"`
		Intent    struct {
			Name  string `json:"name"`
			Slots struct {
				PostalAddress struct {
					Name  string
					Value string
				} `json:"PostalAddress"`
				City struct {
					Name  string
					Value string
				} `json:"City"`
				State struct {
					Name  string
					Value string
				} `json:"State"`
			} `json:"slots"`
		} `json:"intent"`
	} `json:"request"`
}

type AlexaResOutputSpeech struct {
	Type string `json:"type"`
	Text string `json:"text,omitempty"`
	Ssml string `json:"ssml,omitempty"`
}

type AlexaResponse struct {
	Version  string `json:"version"`
	Response struct {
		OutputSpeech     *AlexaResOutputSpeech `json:"outputSpeech,omitempty"`
		ShouldEndSession bool                  `json:"shouldEndSession"`
	} `json:"response"`
}
