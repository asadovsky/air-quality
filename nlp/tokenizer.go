package main

import (
	"bufio"
	"fmt"
	"io"
	"strings"
)

// Token is the unit of tokenization for a grammar.
type Token struct {
	Type TokenType
	Data string
}

// TokenType is the type of a Token.
type TokenType uint32

const (
	// TextToken is a terminal node, e.g. "text".
	TextToken = iota
	// RuleToken is a rule node, e.g. "$rule".
	RuleToken
	// CommentToken is a comment, e.g. "/* bar\nbaz */" or "// bar baz".
	CommentToken
	// OpenParenToken is "(".
	OpenParenToken
	// CloseParenToken is ")".
	CloseParenToken
	// OpenBracketToken is "[".
	OpenBracketToken
	// CloseBracketToken is "]".
	CloseBracketToken
	// OrToken is "|".
	OrToken
	// EqualSignToken is "=".
	EqualSignToken
	// SemicolonToken is ";".
	SemicolonToken
	// NewlineToken is "\n".
	NewlineToken
)

// Scanner is a grammar tokenizer.
// TODO: Maintain and expose current position (line and column).
type Scanner struct {
	r *bufio.Reader
}

func NewScanner(r io.Reader) *Scanner {
	return &Scanner{r: bufio.NewReader(r)}
}

func isLetter(b byte) bool {
	return b >= byte('a') && b <= byte('z') || b >= byte('A') && b <= byte('Z')
}

func isNumber(b byte) bool {
	return b >= byte('0') && b <= byte('9')
}

func isValidTextByte(b byte) bool {
	return isLetter(b) || isNumber(b) || strings.IndexByte("'-.:@{}", b) != -1
}

func makeUnexpectedByteError(b byte) error {
	return fmt.Errorf("Unexpected byte: %s", string(b))
}

// readTokenSuffix reads a token, starting from the second byte.
func readTokenSuffix(r *bufio.Reader, isValid func(byte) bool) ([]byte, error) {
	buf := []byte{}
	for {
		peek, err := r.Peek(1)
		if err != nil {
			if err == io.EOF {
				return buf, nil
			}
			return nil, err
		}
		b := peek[0]
		if strings.IndexByte("()[]|=; ", b) != -1 {
			return buf, nil
		}
		if isValid(b) {
			r.ReadByte()
			buf = append(buf, b)
			continue
		}
		return nil, makeUnexpectedByteError(b)
	}
}

// readRuleSuffix reads a RuleToken, starting from the second byte.
func readRuleSuffix(r *bufio.Reader) ([]byte, error) {
	return readTokenSuffix(r, isLetter)
}

// readTextSuffix reads a TextToken, starting from the second byte.
func readTextSuffix(r *bufio.Reader) ([]byte, error) {
	return readTokenSuffix(r, isValidTextByte)
}

// Next returns the next token. Once Next returns an error, it must never be
// called again.
func (s *Scanner) Next() (Token, error) {
	var b byte
	var err error
	for {
		b, err = s.r.ReadByte()
		if err != nil {
			return Token{}, err
		}
		if b != byte(' ') {
			break
		}
	}
	// TODO: Handle comments.
	switch b {
	case byte('('):
		return Token{OpenParenToken, string(b)}, nil
	case byte(')'):
		return Token{CloseParenToken, string(b)}, nil
	case byte('['):
		return Token{OpenBracketToken, string(b)}, nil
	case byte(']'):
		return Token{CloseBracketToken, string(b)}, nil
	case byte('|'):
		return Token{OrToken, string(b)}, nil
	case byte('='):
		return Token{EqualSignToken, string(b)}, nil
	case byte(';'):
		return Token{SemicolonToken, string(b)}, nil
	case byte('\n'):
		return Token{NewlineToken, string(b)}, nil
	case byte('$'):
		buf, err := readRuleSuffix(s.r)
		if err != nil {
			return Token{}, err
		}
		return Token{RuleToken, string(append([]byte{b}, buf...))}, nil
	default:
		if !isValidTextByte(b) {
			return Token{}, makeUnexpectedByteError(b)
		}
		buf, err := readTextSuffix(s.r)
		if err != nil {
			return Token{}, err
		}
		return Token{TextToken, string(append([]byte{b}, buf...))}, nil
	}
}
