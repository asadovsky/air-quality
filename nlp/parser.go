package main

import (
	"bytes"
	"errors"
	"fmt"
	"io"
)

// Node is a node in a grammar rule.
type Node struct {
	// Root node's Type is SequenceNode.
	Type NodeType
	// Root node's Parent is nil.
	Parent *Node
	// Empty for TextNodes and RuleNodes.
	Children []*Node
	// Text for TextNode; rule name for RuleNode; empty for all other node types.
	Data string
}

// NodeType is the type of a Node.
type NodeType uint32

const (
	// TextNode is a terminal node, e.g. "text".
	TextNode = iota
	// RuleNode is a rule node, e.g. "$rule".
	RuleNode
	// GroupNode is "(...)". Such nodes have >= 0 children.
	GroupNode
	// OptionalNode is "[...]". Such nodes have >= 0 children.
	OptionalNode
	// OrNode is "x | y". Such nodes have exactly 2 children, both SequenceNodes.
	OrNode
	// SequenceNode simplifies OrNode parsing. It behaves like GroupNode.
	SequenceNode
)

// Grammar is a set of rules.
type Grammar struct {
	Rules map[string]*Node
}

func writeString(w io.Writer, s string) (n int64, err error) {
	wn, err := io.WriteString(w, s)
	return int64(wn), err
}

func writeStringN(w io.Writer, s string, n *int64) error {
	wn, err := writeString(w, s)
	*n += wn
	return err
}

func (node *Node) writeChildrenTo(w io.Writer, prefix, suffix string) (n int64, err error) {
	if err = writeStringN(w, prefix, &n); err != nil {
		return
	}
	for i, child := range node.Children {
		if i != 0 {
			if err = writeStringN(w, " ", &n); err != nil {
				return
			}
		}
		var wn int64
		wn, err = child.WriteTo(w)
		n += wn
		if err != nil {
			return
		}
	}
	err = writeStringN(w, suffix, &n)
	return
}

// WriteTo writes a string representation of this Node to the given Writer.
func (node *Node) WriteTo(w io.Writer) (n int64, err error) {
	switch node.Type {
	case TextNode:
		return writeString(w, node.Data)
	case RuleNode:
		return writeString(w, node.Data)
	case GroupNode:
		return node.writeChildrenTo(w, "(", ")")
	case OptionalNode:
		return node.writeChildrenTo(w, "[", "]")
	case OrNode:
		var wn int64
		wn, err = node.Children[0].WriteTo(w)
		n += wn
		if err != nil {
			return
		}
		if err = writeStringN(w, " | ", &n); err != nil {
			return
		}
		wn, err = node.Children[1].WriteTo(w)
		n += wn
		return
	case SequenceNode:
		return node.writeChildrenTo(w, "", "")
	default:
		return 0, fmt.Errorf("Unknown NodeType %d", node.Type)
	}
}

// String returns a string representation of this Node.
func (node *Node) String() string {
	buf := bytes.Buffer{}
	if _, err := node.WriteTo(&buf); err != nil {
		panic(err)
	}
	return buf.String()
}

// WriteTo writes a string representation of this Grammar to the given Writer.
func (g *Grammar) WriteTo(w io.Writer) (n int64, err error) {
	for name, node := range g.Rules {
		if err = writeStringN(w, name, &n); err != nil {
			return
		}
		if err = writeStringN(w, " = ", &n); err != nil {
			return
		}
		var wn int64
		wn, err = node.WriteTo(w)
		n += wn
		if err != nil {
			return
		}
		if err = writeStringN(w, ";\n", &n); err != nil {
			return
		}
	}
	return
}

// String returns a string representation of this Grammar.
func (g *Grammar) String() string {
	buf := bytes.Buffer{}
	if _, err := g.WriteTo(&buf); err != nil {
		panic(err)
	}
	return buf.String()
}

func makeUnexpectedTokenError(t Token) error {
	return fmt.Errorf("Unexpected token: %s", t.Data)
}

// consumeToken consumes the next token, which must be of the given type.
func consumeToken(s *Scanner, tt TokenType) error {
	t, err := s.Next()
	if err != nil {
		return err
	}
	if t.Type != tt {
		return makeUnexpectedTokenError(t)
	}
	return nil
}

func closeOrNodes(node *Node) (*Node, error) {
	for node.Type == SequenceNode && node.Parent != nil {
		if len(node.Children) == 0 {
			return nil, errors.New("OrNode has empty RHS")
		}
		if node.Parent.Type != OrNode || node.Parent.Parent == nil {
			return nil, errors.New("Invariant violation")
		}
		node = node.Parent.Parent
	}
	return node, nil
}

// parseRuleExpansion parses a rule expansion, consuming tokens up to and
// including the semicolon.
func parseRuleExpansion(s *Scanner) (*Node, error) {
	node := &Node{SequenceNode, nil, []*Node{}, ""}
	for {
		t, err := s.Next()
		if err != nil {
			return nil, err
		}
		// TODO: Handle CommentToken.
		switch t.Type {
		case TextToken:
			node.Children = append(node.Children, &Node{TextNode, nil, nil, t.Data})
		case RuleToken:
			node.Children = append(node.Children, &Node{RuleNode, nil, nil, t.Data})
		case OpenParenToken:
			child := &Node{GroupNode, node, []*Node{}, ""}
			node.Children = append(node.Children, child)
			node = child
		case CloseParenToken:
			if node, err = closeOrNodes(node); err != nil {
				return nil, makeUnexpectedTokenError(t)
			}
			if node.Type != GroupNode {
				return nil, makeUnexpectedTokenError(t)
			}
			node = node.Parent
		case OpenBracketToken:
			child := &Node{OptionalNode, node, []*Node{}, ""}
			node.Children = append(node.Children, child)
			node = child
		case CloseBracketToken:
			if node, err = closeOrNodes(node); err != nil {
				return nil, makeUnexpectedTokenError(t)
			}
			if node.Type != OptionalNode {
				return nil, makeUnexpectedTokenError(t)
			}
			node = node.Parent
		case OrToken:
			if len(node.Children) == 0 { // OrNode has empty LHS
				return nil, makeUnexpectedTokenError(t)
			}
			orNode := &Node{OrNode, node, nil, ""}
			lhs := &Node{SequenceNode, orNode, node.Children, ""}
			rhs := &Node{SequenceNode, orNode, []*Node{}, ""}
			orNode.Children = []*Node{lhs, rhs}
			node.Children = []*Node{orNode}
			node = rhs
		case SemicolonToken:
			if node, err = closeOrNodes(node); err != nil {
				return nil, makeUnexpectedTokenError(t)
			}
			if node.Parent != nil || len(node.Children) == 0 {
				return nil, makeUnexpectedTokenError(t)
			}
			return node, nil
		default:
			return nil, makeUnexpectedTokenError(t)
		}
	}
}

// consumeRestOfLine consumes any tokens (e.g. comments) after a rule
// definition, up to and including the newline. May return io.EOF.
func consumeRestOfLine(s *Scanner) error {
	for {
		t, err := s.Next()
		if err != nil {
			return err
		}
		switch t.Type {
		case NewlineToken:
			return nil
		case CommentToken:
			continue
		default:
			return makeUnexpectedTokenError(t)
		}
	}
}

// Parse parses the given input into a grammar.
func Parse(r io.Reader) (*Grammar, error) {
	g := &Grammar{Rules: map[string]*Node{}}
	s := NewScanner(r)
	for {
		t, err := s.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
		// TODO: Handle CommentToken.
		switch t.Type {
		case NewlineToken:
			continue
		case RuleToken:
			if _, ok := g.Rules[t.Data]; ok {
				return nil, fmt.Errorf("Duplicate rule: %s", t.Data)
			}
			if err := consumeToken(s, EqualSignToken); err != nil {
				return nil, err
			}
			node, err := parseRuleExpansion(s)
			if err != nil {
				return nil, err
			}
			g.Rules[t.Data] = node
			if err := consumeRestOfLine(s); err == io.EOF {
				break
			} else if err != nil {
				return nil, err
			}
		default:
			return nil, makeUnexpectedTokenError(t)
		}
	}
	// TODO: Check that all rule references are valid.
	if _, ok := g.Rules["$root"]; !ok {
		return nil, errors.New("No root rule")
	}
	return g, nil
}
