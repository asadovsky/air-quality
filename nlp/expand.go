// To run:
// echo -e '$root = (a|b) [c|d|e] $f;\n$f = g;' | ./nlp
// ./nlp < grammar.txt

package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
)

func multiplyExpansions(g *Grammar, nodes []*Node, prefix string) []string {
	if len(nodes) == 0 {
		return []string{}
	}
	exps := getExpansionsForNode(g, nodes[0])
	if len(prefix) > 0 {
		for i, _ := range exps {
			if len(exps[i]) > 0 {
				exps[i] = prefix + " " + exps[i]
			} else {
				exps[i] = prefix
			}
		}
	}
	if len(nodes) == 1 {
		return exps
	}
	// At this point, exps is the set of possible prefixes.
	res := []string{}
	for _, exp := range exps {
		res = append(res, multiplyExpansions(g, nodes[1:], exp)...)
	}
	return res
}

func getExpansionsForNode(g *Grammar, node *Node) []string {
	switch node.Type {
	case TextNode:
		return []string{node.Data}
	case RuleNode:
		return getExpansionsForNode(g, g.Rules[node.Data])
	case GroupNode:
		return multiplyExpansions(g, node.Children, "")
	case OptionalNode:
		return append([]string{""}, multiplyExpansions(g, node.Children, "")...)
	case OrNode:
		exps := []string{}
		for _, child := range node.Children {
			exps = append(exps, getExpansionsForNode(g, child)...)
		}
		return exps
	case SequenceNode:
		return multiplyExpansions(g, node.Children, "")
	default:
		panic(fmt.Errorf("Unknown NodeType %d", node.Type))
	}
}

func getExpansions(g *Grammar) []string {
	return getExpansionsForNode(g, g.Rules["$root"])
}

func main() {
	b, err := ioutil.ReadAll(os.Stdin)
	if err != nil {
		panic(err)
	}
	g, err := Parse(bytes.NewReader(b))
	if err != nil {
		panic(err)
	}
	if false {
		fmt.Print(g.String())
	} else {
		for _, exp := range getExpansions(g) {
			fmt.Println(exp)
		}
	}
}
