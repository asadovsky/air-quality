curl -X POST http://localhost:8080/api/dialogflow-v1 -d '{"result": {"action": "GetAirQuality", "parameters": {"geo-city": "santa monica"}}}' -H "Content-Type: application/json"

curl -X POST http://localhost:8080/api/dialogflow-v2 -d '{"queryResult": {"action": "GetAirQuality", "parameters": {"geo-city": "santa monica"}}}' -H "Content-Type: application/json"
