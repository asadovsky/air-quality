// python -m SimpleHTTPServer 8001
// http://localhost:8001/index.html

var CLICK = true;

function addImage(src, width, height) {
  var image = new Image();
  image.src = src;
  image.onload = function() {
    var c = document.createElement('canvas');
    document.body.appendChild(c);

    c.width = width || image.width;
    c.height = height || image.height;
    c.getContext('2d').drawImage(image, 0, 0, c.width, c.height);

    var a = document.createElement('a');
    a.download = src.split('.')[0] + '_' + c.width + 'x' + c.height + '.png';
    a.href = c.toDataURL('image/png');
    if (CLICK) {
      a.dispatchEvent(new MouseEvent('click', {
        view: window,
        bubbles: true,
        cancelable: true
      }));
    }
  };
}

// For Actions on Google.
addImage('large_rect.svg', 1920, 1080);
addImage('small_square.svg', 192, 192);

// For Alexa.
addImage('small_square.svg', 108, 108);
addImage('small_square.svg', 512, 512);
