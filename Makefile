SHELL := /bin/bash -euo pipefail
PATH := node_modules/.bin:$(PATH)
PROJECT := air-quality-13001

.DELETE_ON_ERROR:

all: build

nlp/nlp:
	cd nlp && go build

.PHONY: build
build:

########################################
# AppEngine commands

.PHONY: serve
serve: build
	dev_appserver.py --clear_datastore=1 app/app.yaml

.PHONY: deploy
deploy: build
	gcloud app deploy --project=$(PROJECT) app/*.yaml

.PHONY: deploy-dev
deploy-dev: build
	gcloud app deploy --project=$(PROJECT) app/*.yaml --version=dev --no-promote

.PHONY: gc-no-traffic
gc-no-traffic:
	gcloud app versions delete --project=$(PROJECT) `gcloud app versions list --project=$(PROJECT) --format='csv[no-heading](VERSION.ID)' --filter='TRAFFIC_SPLIT=0 AND VERSION.ID!=dev'`

.PHONY: get-secrets
get-secrets:
	TMPDIR=`mktemp -d` && appcfg.py -A $(PROJECT) download_app "$$TMPDIR" && cp "$$TMPDIR/secrets.go" app/ && rm -rf "$$TMPDIR"

########################################
# Clean and lint

.PHONY: clean
clean:
	rm nlp/nlp

.PHONY: lint
lint:
	go vet ./{app,nlp}/...
